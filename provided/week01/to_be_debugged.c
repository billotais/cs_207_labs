/**
 * @file to_be_debugged.c
 * @brief A nasty C code that does not exactly what it should.
 *
 * Computes the average and the standard deviation of the number of
 * credits taken by some students.
 *
 * @author J.-C. Chappelier
 * @date 10 Fev 2017
 */

#include <stdio.h>
#include <math.h> // for sqrt()

int main(void)
{
    const int maximum_students = 100;
    int credits[maximum_students];

    int nb_students = 0;
    do {
        printf("Combien d'étudiants (<= %d) ? ", maximum_students);
        scanf("%d", &nb_students); /* We will assume that a int is typed in.
                                    * We do not bother I/O errors for the
                                    * moment (week < 4).
                                    */
    } while (nb_students >= maximum_students);

    int somme;
    int somme2;
    for (int i = 0; i < maximum_students; ++i) {
        printf("Nombre de crédits de l'étudiant %d : ", i+1);
        scanf("%d", &credits[i]); // no error in that line
        somme  += credits[i];
        somme2 += credits[i] * credits[i];
    }

    double moyenne  = somme  / nb_students;
    double moyenne2 = somme2 / nb_students;
    double ecart_type = sqrt( (nb_students / (nb_students - 1.0)) // biais correction
                              * (moyenne2 - moyenne * moyenne) );

    printf("\nMoyenne    : %g\n", moyenne);
    printf("Ecart-type : %g\n", ecart_type);

    return 0;
}
