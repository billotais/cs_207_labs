# Projet programmation système W01 - Suite...

## manpages

Les manpages sont **LA** documentation de référence dans les mondes Unix. Elles peuvent se lire avec la commande `man` (ou sont [aussi accessibles en ligne](http://man7.org/linux/man-pages/)).

La première manpage à lire (peut-être pas en entier `;-)`, mais en tout cas à parcourir) est peut-être la manpage de la commande `man` elle-même : tapez

    man man

Appuyez sur « Espace » pour défiler les pages et sur 'q' (ou 'Q') pour sortir. Appuyez sur 'h' pour avoir de l'aide (**h** elp).

`man` utilise un « page viewer » pour afficher son contenu. Sur la plupart des systèmes Unix actuels, le « page viewer » utilisé est `less` (remplaçant de `more` ; ça ne s'invente pas !). La seconde manpage à lire me semble donc être celle de `less` :

    man less

Une des premières fonctions C que vous allez rencontrer en cours dès mercredi, mais sans qu'elle soit détaillée, est la fonction `printf`. Essayons de voir ce que les manpages nous en disent :

    man printf

Ah ?... Ca ne semble pas être le bon `printf`...
Si vous avez « PRINTF(1) » en haut de la page ce n'est effectivement pas le bon.

En effet, il peut exister plusieurs manpages pour plusieurs commandes (ou même contextes) différent(e)s mais de même nom. Pour les différencier, les manpages sont organisées par « sections ».
Allez relire la manpage de man si cette notion importante de section vous a échappé :

    man man

Ensuite allez lire la « bonne » manpage de `printf`. Pour information, elle se trouve dans la section 3 (c'est à vous de chercher comment faire : relisez si nécessaire la manpage de `man`).

Allez aussi lire la manpage de `puts`, puis passez à la suite de ce TP lorsque vous pensez en savoir assez sur les manpages.  
Et n'oubliez pas utiliser `man` dès que vous aurez besoin d'information sur une commande, une bibliothèque, un format, etc.

## Compilation du premier programme en C

Le but de cet exercice est de vérifier que votre environnement de développement C est en place.
Pour cela, vous devriez avoir un fichier `hello_word.c` récupéré depuis votre dépôt GitHub.

Pour le compiler, tapez simplement :

    gcc -o hello_world hello_world.c

(ou avec `clang` si vous préférez ce compilateur là).

Vérfiez que la compilation a produit un exécutable ; avec la commande :

    ls

vous devriez voir un nouveau fichier, appelé simplement `hello_world`.

Pour l'exécuter, tapez la commande :

    ./hello_world

ce qui devrait vous afficher un gentil message.

Votre but est maintenant de traduire ce message en français.

**NOTE :** Attention à ne **jamais** modifier le répertoire `provided` !

Pour cela, créez à la racine de votre dépot Git un nouveau répertoire `done` dans lequel vous créez un sous-répertoire `week01`.  
Puis copiez le programme `hello_word.c` depuis `provided/week01` vers `done/week01` et éditez ce dernier.  
Modifiez ce qu'il faut ; compilez et vérifiez.

Si tout fonctionne bien, « commitez » (et « poussez ») votre modification dans votre dépôt GutHub.  
N'oubliez pas de commenter votre « commit » !

**NOTE :** Nous utiliserons pour ce cours la norme C99 ; pour certains compilateurs, la compilation de certains codes fournis peut alors nécessiter l'ajout de l'option de compilation `-std=c99`.


## Documentation avec Doxygen

Lors de l'édition du fichier `hello_word.c`, vous avez peut être remarqué qu'il est commenté (_toujours_ commenter vos programmes !), dans un format un peu particulier (« c'est quoi ces `@` ? »).  
Ce n'est pas que pour faire joli ; c'est aussi utile !..

Tapez :

    doxygen -g
    doxygen Doxyfile

puis regadez le fichier `html/index.html` dans votre navigateur préféré.  
Cliquez sur « Files », puis sur « hello_world.c ».  
Cool, non ?

Nettoyez tout ça avec la commande

    rm -r latex html

Dans le futur, pensez à documenter vos codes avec des commentaires compatibles avec Doxygen.
Des exemples vous seront fournis, mais si vous voulez en savoir plus, naviguez sur [le site de Doxygen](http://www.stack.nl/~dimitri/doxygen/).

## Utilisation d'un débogueur

Voir [ce tutoriel](http://progos.epfl.ch/projet/handouts/gdb.md) pour l'utilisation du débogueur `gdb`.

Cet outil (ou un autre similaire) pourra vous être utile pour corriger le code `to_be_debugged.c` fourni.

Le devoir à rendre pour cette semaine (délai : dimanche 05 mars 23:59) est la version entièrement corrigée de ce code dont le but est de calculer la moyenne et l'écart-type (non biaisé) des crédits pris par un ensemble de 1 à 100 étudiants (attention ! il contient _plusieurs_ erreurs, de différente nature ; il n'y a par contre pas d'erreur de nature mathématique : les formules sont correctes du point de vue mathématique ; notez enfin que l'écart-type d'une population réduite à un seul individu est zéro).  
Le code rendu doit être correct et robuste à toute entrée de type entier (mais nous ne vous demandons pas de traiter le cas d'entrées non entières composées de caractères quelconques ; nous ne testerons votre code qu'avec des entiers, positifs ou négatifs).

Pour rendre le devoir, renommez-le `debugged.c` et ajoutez-le à votre GitHub dans un répertoire `done/week01` (à la racine de votre gitHub, **PAS** dans `provided` ; vous ne devez **jamais** modifier le répertoire `provided`) ; puis `commit` et `push`.

Quand vous estimez avoir tout fini et devoir faire le rendu (faites le avant dimanche 05 mars 23:59),
rendez officiellement votre devoir en faisant

    git tag week01

**<span style="color: red">ATTENTION ! Vous ne pourrez pas annuler votre rendu, c.-à-d. vous ne pourrez pas supprimer/déplacer votre tag.</span>**

Pour pousser le tag vers GitHub, ajoutez `--tags` au push:

    git push --tags


## Make et Makefiles

L'outil `make` sert (entre autres) à compiler de gros projets. Il vous sera utile plus tard dans votre projet (même si celui-ci ne sera pas si gros `:-)`).

Voir [ici](http://progos.epfl.ch/projet/handouts/make.md) pour en savoir plus sur `make` et les `Makefile`s.

### Pratique 1

Ecrivez un `Makefile` pour compiler le programme fourni (`to_be_debugged.c`).

([Solution ici](http://progos.epfl.ch/projet/handouts/solution01a.md)).

### Pratique 2

Modifiez le `Makefile` précédent pour compiler avec l'option `-g`.

([Solution ici](http://progos.epfl.ch/projet/handouts/solution01b.md)).


## Conclusion

Voilà, c'est tout pour cette semaine où nous avons voulu mettre en place et vous faire découvrir les outils de développement nécessaires à notre projet et très souvent utiles dans les projets de programmation système.  
L'essentiel du travail de cette semaine était donc pour vous, pour votre
développement personnel.

La seule chose que vous ayez à rendre, d'ici le dimanche 05 mars 23:59, est le fichier `debugged.c` (correction du fichier `to_be_debugged.c` fourni), à mettre dans votre dépôt GitHub dans le répertoire `done/week01` et à « étiquetter » du tag `week01`. N'oubliez pas de « pousser » (`git push`) le tout.
