/**
 * @file swc.c
 * @brief A sort of word-count (wc).
 *
 * Computes the number of lines, words and bytes of a list of files.
 *
 * @author Mia Primorac and Jean-Cédric Chappelier
 * @date 2 Nov 2015
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> // for isspace

/**
 * @brief Basic datastructure to store line, word, and byte counts.
 *
 */

/* **********************************************************************
 * TODO: Write the counts datastructure.
 * **********************************************************************
 */

/********************************************************************//**
 *
 * @brief Computes the number of lines, words and bytes of a files.
 *
 * @param fp The (opened) file to count from.
 * @return the number of lines, words and bytes counts.
 *
 *//********************************************************************/

/* **********************************************************************
 * TODO: Write the count function
 * **********************************************************************
 */

/********************************************************************//**
 *
 * @brief Opens a file readonly. Handles errors.
 *
 * @param filename the name of the file to be opened.
 * @return the opened file.
 *
 *//********************************************************************/
FILE*  open_file_readonly(const char* filename)
{
    if (filename == NULL) {
        return NULL;
    }
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, "ERROR: cannot open file \"%s\"\n", filename);
    }
    return file;
}

/********************************************************************//**
 *
 * @brief wc-like formated printinf of counts
 *
 * @param filename the name of the handled file
 * @param wc the counts
 *
 *//********************************************************************/
void print_counts(const char* filename, const counts wc)
{
    printf("%4lu %4lu %4lu %s\n",
           /* **********************************************************************
            * TODO: Complete here to print the desired counts.
            * **********************************************************************
            */
           filename);
}

/* ******************************************************************
   ****************************************************************** */
int main (int argc, char* argv[])
{

    /* **********************************************************************
     * TODO: Complete, where needed, so as to compute the total counts.
     * **********************************************************************
     */

    if (argc < 2) {
        fprintf(stderr, "Not enough arguments.\nUsage: ./swc FILE [FILE...]\n");
        return 1;
    }
    int i = 0;
    while (++i < argc) {
        FILE* fp = open_file_readonly(argv[i]);
        if (fp != NULL) {
            counts wc = count(fp);
            fclose(fp);
            print_counts(argv[i], wc);
        }
    }
    return 0;
}
