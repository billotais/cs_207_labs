/**
 * @file swc.c
 * @brief A sort of word-count (wc).
 *
 * Computes the number of lines, words and bytes of a list of files.
 *
 * @author Mia Primorac and Jean-Cédric Chappelier
 * @date 2 Nov 2015
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h> // for isspace

/**
 * @brief Basic datastructure to store line, word, and byte counts.
 *
 */

typedef struct {
    unsigned long int lines;
    unsigned long int words;
    unsigned long int bytes;
} counts;
/********************************************************************//**
 *
 * @brief Computes the number of lines, words and bytes of a files.
 *
 * @param fp The (opened) file to count from.
 * @return the number of lines, words and bytes counts.
 *
 *//********************************************************************/

counts count(FILE* file) {
    int charRead;
    int ligneNbr = 0;
    int wordNbr = 0;
    int charNbr = 0;
    int lastWasSpace = 1;
    while ((charRead = fgetc(file)) != EOF) {

        charNbr += 1;
        if (charRead == '\n') {
            ligneNbr += 1;
        }
        if (!isspace(charRead) && lastWasSpace == 1) {
            wordNbr += 1;
            lastWasSpace = 0;
        }
        else if (isspace(charRead)) {
            lastWasSpace = 1;
        }
    }
    counts stats = {ligneNbr, wordNbr, charNbr};
    return stats;

}

/********************************************************************//**
 *
 * @brief Opens a file readonly. Handles errors.
 *
 * @param filename the name of the file to be opened.
 * @return the opened file.
 *
 *//********************************************************************/
FILE*  open_file_readonly(const char* filename)
{
    if (filename == NULL) {
        return NULL;
    }
    FILE* file = fopen(filename, "r");
    if (file == NULL) {
        fprintf(stderr, "ERROR: cannot open file \"%s\"\n", filename);
    }
    return file;
}

/********************************************************************//**
 *
 * @brief wc-like formated printinf of counts
 *
 * @param filename the name of the handled file
 * @param wc the counts
 *
 *//********************************************************************/
void print_counts(const char* filename, const counts wc)
{
    printf("%4lu %4lu %4lu %s\n",
           wc.lines, wc.words, wc.bytes,
           filename);
}

/* ******************************************************************
   ****************************************************************** */
int main (int argc, char* argv[])
{

    /* **********************************************************************
     * TODO: Complete, where needed, so as to compute the total counts.
     * **********************************************************************
     */

    if (argc < 2) {
        fprintf(stderr, "Not enough arguments.\nUsage: ./swc FILE [FILE...]\n");
        return 1;
    }
    int i = 0;
    unsigned long int totLignes = 0;
    unsigned long int totWords = 0;
    unsigned long int totChars = 0;
    while (++i < argc) {
        FILE* fp = open_file_readonly(argv[i]);
        if (fp != NULL) {
            counts wc = count(fp);
            fclose(fp);
            print_counts(argv[i], wc);
            totLignes += wc.lines;
            totWords += wc.words;
            totChars += wc.bytes;
            
        }
    }
    printf("%4lu %4lu %4lu total\n",
           totLignes, totWords, totChars);
    return 0;
}
