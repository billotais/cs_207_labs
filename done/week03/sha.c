#include <stdio.h>
#include <openssl/sha.h>



// Print the value of tab considering it represents a sha

void print_sha(unsigned char tab[]) {
  if (tab != NULL) {
    for (int i = 0; i < SHA256_DIGEST_LENGTH; ++i) {
      printf("%02x", tab[i]);
    }
  }
}

// Create and print a sha of a given array

void print_sha_from_content(unsigned char tab[], int size) {
  unsigned char result[SHA256_DIGEST_LENGTH];
  print_sha(SHA256(tab, size, result));
}
