/**
 * @file sha.c
 * @brief A simple example usage of SSL SHA.
 *
 * @author J.-C Chappelier and Mia Primorac
 * @date 16 Feb 2016
 */

#include <stdio.h>
#include <string.h> // for strlen()
#include "sha.h"

#define MAX_INPUT_SIZE 256

// ======================================================================
int main(void)
{
  
    char input[MAX_INPUT_SIZE] = "";
    fputs("Entrez une phrase : ", stdout);
    if (fgets(input, MAX_INPUT_SIZE, stdin) != NULL)
      if (strlen(input) != 0) input[strlen(input)-1] = '\0';

    puts("Le SHA256 de");
    printf("\"%s\"\n", input);
    puts("est : ");
    print_sha_from_content((unsigned char *)input, strlen(input));
    putchar('\n');

    return 0;
}
