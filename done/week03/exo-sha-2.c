#include <stdio.h>
#include "sha.h"
#include <openssl/sha.h>

// Prototypes

void zero(unsigned char tab[], int size);
void print_content(unsigned char tab[], int size);
int print_match(unsigned char a[], int sizeA, unsigned	char b[], int sizeB);
void lookfor (int generate_size, unsigned char a[], int sizeA);


int main(void) {

	// Create a new array and fill it with 0s

	unsigned char tabOriginal[100];
	zero(tabOriginal, 100);

	// Compute the SHA of the empty array

	unsigned char originalSHA[SHA256_DIGEST_LENGTH];
	SHA256(tabOriginal, 100, originalSHA);

	// Print the SHA array

	printf("Initial SHA256 : \n");
	print_content(originalSHA, SHA256_DIGEST_LENGTH);

	printf("\n====== Check size : 1 ======\n\n");

	// Look for 1 / 100 byte words with same starting byte as original sha

	printf("------ Stratégie 1 -------\n");
	lookfor(1, originalSHA, 1);

	printf("------ Stratégie 2 : ------\n");
	lookfor(100, originalSHA, 1);

	printf("\n====== Check size : 2 ======\n\n");


	// Look for 1 / 100 byte words with same startng 2 bytes as original sha

	printf("------ Stratégie 1 ------\n");
	lookfor(1, originalSHA, 2);

	printf("------ Stratégie 2 ------\n");
	lookfor(100, originalSHA, 2);

	return 0;
}

// Set all elements of an array to 0

void zero(unsigned char tab[], int size) {
	for (int i = 0; i < size; ++i) {
		tab[i] = 0;
	}
}

// Print the content of an array

void print_content(unsigned char tab[], int size) {
	for (int i = 0; i < size; ++i) {
		printf("%02x", tab[i]);
	}
	printf("\n");
}
// Check if first sizaA bytes of sha(b) correspond to first sizeA bytes of a

int print_match(unsigned char a[], int sizeA, unsigned	char b[], int sizeB) {

	// Create SHA of b[]

	unsigned char shaOfB[SHA256_DIGEST_LENGTH];
	SHA256(b, sizeB, shaOfB);

	// Check if the sizeA first elements are the same

	for (int i = 0; i < sizeA; ++i) {
		if (a[i] != shaOfB[i]){ return 0;}
	}

	// Print the sha
	print_content(shaOfB, SHA256_DIGEST_LENGTH);
	return 1;
}

// Look for wors of generate_size bytes if similarities to a[]

void lookfor (int generate_size, unsigned char a[], int sizeA) {

	// Initialize new array to 0 and counts to 0

	unsigned char content[generate_size];
	int count = 0;
	int succes = 0;
	for (int i = 0; i < generate_size; ++i) {
		content[i] = 0;
	}

	// For each byte < generate_size, try all possible values

	for (int j = 0; j < generate_size; ++j) {
		for (int l = 1; l < 256; ++l) {
			content[j] = l;
			succes += print_match(a, sizeA, content, generate_size);
			count += 1;
		}
		content[j] = 0;
	}

	// Print result

	printf("%d matchs over %d tests\n", succes, count);
}
